// ABR_H
#ifndef ABR_H
#define ABR_H

#include <stdio.h>

typedef struct noeud{
  int val;
  struct noeud *fg, *fd;
}Noeud, *AbrBin;

typedef enum {FAUX,VRAI}Booleen;

Booleen insererFeuilles(AbrBin *abr,int val);
Booleen supprimerAbr(AbrBin *abr);
Booleen estAbrVide(AbrBin abr);
Noeud* rechercherVal(AbrBin abr,int val);
Booleen supprimerVal(AbrBin *abr, int val);
void afficherCroissant(AbrBin abr);
void afficherDecroissant(AbrBin abr);
Booleen triCroissant(AbrBin abr, int **tab);
AbrBin creerAbrVide();
  
#endif // ABR_H
