// abr.c
#include "abr.h"

AbrBin creerAbrVide(){
  AbrBin abr;
  return abr=NULL;
}

Booleen insererFeuilles(AbrBin *abr,int val){
  Noeud *tmp=(Noeud*)malloc(sizeof(Noeud));
  if(tmp==NULL) return FAUX;
  tmp->val=val;
  tmp->fg=NULL;
  tmp->fd=NULL;
  if(AbrBin==NULL){
    abr=tmp;
  }
  if(AbrBin->val > val){
    if(AbrBin->fg!=NULL){
      insererFeuilles(abr->fg,val);
    }
    abr->fg=tmp;
  }
  if(AbrBin->val < val){
    if(AbrBin->fd!=NULL){
      insererFeuilles(abr->fd,val);
    }
    abr->fd=tmp;
  }
  return VRAI;
}

Booleen supprimerAbr(AbrBin *abr){
  free(abr);
  return;
}

Booleen estAbrVide(AbrBin abr){
  if(abr==NULL)return VRAI;
  return FAUX;
}

Noeud* rechercherVal(AbrBin abr,int val){
  Noeud *tmp;
  if(estAbrVide(abr))return NULL;
  if(abr->val==val) return abr;
  tmp=rechercherVal(abr->fg,val);
  if(tmp==NULL) tmp=rechercher(abr->fg,val);
  return tmp;
}

Booleen supprimerVal(AbrBin *abr, int val){ //a terminer
  AbrBin *tmp=(AbrBin*)malloc(sizeof(AbrBin));
  if(tmp==NULL) return FAUX;
  tmp=abr;

  }
  abr=tmp;
  free(tmp);
  return VRAI;
}

void afficherCroissant(AbrBin abr){
  printf("%d\n",abr->val);
  afficherCroissant(abr->fg);
  afficherCroissant(abr->fd);
}

void afficherDecroissant(AbrBin abr){
  afficherCroissant(abr->fg);
  afficherCroissant(abr->fd);
  printf("%d\n",abr->val);
}

Booleen triCroissant(AbrBin abr, int **tab){
  if(tab==NULL) printf("vide\n");
  int i=0;
  Booleen bool;
  while(tab[i]!=NULL){
    bool=insererFeuilles(&abr,tab[i]);
    if(!bool)break;
    i++;
  }
  afficherCroissant(abr);
  return bool;
}

