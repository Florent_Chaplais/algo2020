Contenu du répertoire JeuDeCartes:
  -- doc
       // la documentation (ouvrir index.html)
  -- include   
     -- carte.h
  -- lib        
     -- libcarte.so
  -- Makefile
  -- src
     -- jeu_solitaire.c
     -- jeu_solitaire.h
     -- tests.c
     // et tous les autres fichiers que vous coderez
  -- sujet
     // le sujet


Travail à faire :
Coder le jeu de solitaire en utilisant les cartes dont on vous fournit la bibliothèque.
  - coder les fichiers :
      - pile_de_cartes.h
      - pile_de_cartes.c
      - deque_de_cartes.h
      - deque_de_cartes.c
  - compléter les fichiers
      - jeu_solitaire.h
      - jeu_solitaire.c
      - tests.c


Pour compiler utiliser le Makefile en étant dans le répertoire JeuSolitaire et pas dans src.
  - commande pour compiler : make
  - commande pour supprimer les fichiers .o et l'exécutable : make clean


Précisions : 

 - avant tout, pour que la librairie fournie puisse être trouvée lors de la compilation il faudra modifier votre fichier .bashrc en écrivant vers la fin de celui-ci les deux lignes suivantes : 

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${HOME}/AlgoAvance/TP/TP1-2-3_solitaire/lib
export LD_LIBRARY_PATH
 
- puis taper dans le terminal :  source ~/.bashrc   pour actualiser la console

 - pour compiler il faudra mettre à jour le Makefile au fur et à mesure des fichiers codés qui seront à compiler. Pour celà vous commenterez (avec un #)  ou décommenterez les .o listés au niveau de OBJ. 


