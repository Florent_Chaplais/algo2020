/**
 * \file carte.h
 * \brief Carte de jeu de cartes style 52 ou 32 cartes. 
 * \author Carine Simon
 * \date 9 juillet 2019
 * \version 0.2
 */

#ifndef CARTE_H
#define CARTE_H


/**
 * \enum Booleen
 * \brief Valeurs booléennes
 * 
 * Les deux valeurs booléennes définies sont VRAI et FAUX
 */

typedef enum{FAUX,VRAI} Booleen;

/**
 * \struct Carte 
 * \brief Carte de type jeu de 52 ou 32 cartes.
 *
 *  Une carte est définie par son nom (par exemple : 1, 2, ..., 10, V, D, R), 
 *    par sa couleur (r: rouge ou n: noir), 
 *    par sa valeur (par exemple: 1, 2, ...., 10, 11, 12, 13), 
 *    et par le fait qu'elle soit face visible ou face cachée.
 */
typedef struct {
  int val;         /**< valeur ou nombre de points (exemple :  entre 1 (pour l'as) et 13 (pour le roi)) */
  char nom[3];     /**< nom de la carte  (exemple :  1, 2, 3, ..., 10, V, D, R) */
  char couleur;    /**< couleur :  r ou n (rouge ou noir) */
  Booleen visible; /**< indique si la carte est posée face visible ou face cachée */
}Carte;


/**
 * \fn Carte carteFaceVisible(char nom[], char couleur, int val);
 * \brief construit une carte face visible de nom, de couleur et de valeur donnés.
 * \param nom le nom de la nouvelle carte (exemple :  1, 2, 3, ..., 10, V, D, R)
 * \param couleur  la couleur de la carte : r ou n (rouge ou noir) 
 * \param val la valeur ou le nombre de points de la carte
 */	
Carte carteFaceVisible(char nom[], char couleur, int val);

/**
 * \fn Carte carteFaceCachee(char nom[], char couleur, int val);
 * \brief construit une carte face cachée de nom, de couleur et de valeur donnés.
 * \param nom le nom de la nouvelle carte (exemple :  1, 2, 3, ..., 10, V, D, R)
 * \param couleur  la couleur de la carte : r ou n (rouge ou noir) 
 * \param val la valeur ou le nombre de points de la carte
 */	
Carte carteFaceCachee(char nom[], char couleur, int val);


/**
 * \fn void afficherCarteVisible(Carte c);
 * \brief Affiche la carte différemment selon si elle est face visible ou face cachée
 *
 * Si la carte est face visible, affiche le nom accolé à la couleur de la carte.
 * Si elle est face non visible, affiche - - 
 * \param c la carte à afficher
 */
void afficherCarteVisible(Carte c);

/**
 * \brief Affiche la valeur de la carte.
 *
 * N'affiche que la valeur de la carte, pas son nom ni sa couleur.
 * \param c la carte à afficher
 */
void afficherValeurCarte(Carte c);

/** 
 * \brief Affiche le nom et la couleur de la carte
 *
 * L'affichage est le même que celle-ci soit face visible ou non 
 * \param c la carte à afficher
 */
void afficherCarte(Carte c);

/**
 * \brief retourne la carte.
 *
 * si elle était face visible, elle est maintenant face cachée, et inversement.
 * \param c la carte à afficher
 */
void tournerCarte(Carte* c);

/**
 * \brief indique si la carte est face visible ou cachée
 * \param c la carte à afficher
 * \return VRAI si la carte est face visible
 */
Booleen estVisible(Carte c);

#endif // CARTE_H
