// deque_de_cartes.c
#include "deque_de_cartes.h"

Deque creerDequeVide(){
  return NULL;
}

Booleen estDequeVide(Deque dk){
  if(dk==NULL) return VRAI;
  return FAUX;
}

void enfilerEnTete(Deque *dk, Carte ct){
  MaillonD *tmp;
  tmp=(MaillonD*) malloc(sizeof(MaillonD));
  if(tmp==NULL){
    printf("\nmalloc\n");
    exit(1);
  }
  
  tmp->ct=ct;
  
  if (estDequeVide(*dk)){
    tmp->suiv=tmp;
    tmp->prec=tmp;
    (*dk)=tmp;
  }
  else{
    tmp->suiv=(*dk)->suiv;
    (*dk)->suiv->prec=tmp;
    tmp->prec=(*dk);
    (*dk)->suiv=tmp;
  }
}

void enfilerEnQueue(Deque *dk, Carte ct){
  MaillonD *tmp;
  tmp=(MaillonD*) malloc(sizeof(MaillonD));
  if(tmp==NULL){
    printf("\nmalloc\n");
    exit(1);
  }
  
  tmp->ct=ct;
  
  if (estDequeVide(*dk)){
    tmp->suiv=tmp;
    tmp->prec=tmp;    
  }
  else{
    tmp->suiv=(*dk);
    tmp->prec=(*dk)->prec;
    (*dk)->prec->suiv=tmp;
    (*dk)->prec=tmp;    
  }
  (*dk)=tmp;
}

Booleen teteDeque(Deque dk, Carte *ct){
  if(estDequeVide(dk)) return FAUX;
  *ct=dk->suiv->ct;	
  return VRAI;
}

Booleen queueDeque(Deque dk, Carte *ct){
  if(estDequeVide(dk)) return FAUX;
  *ct=dk->ct;	
  return VRAI;
}

// revoir
Booleen defilerEnQueue(Deque *dk, Carte *ct){
  MaillonD* tmp;
  if(estDequeVide(*dk)) return FAUX;

  printf("\ndefilerQ\n");

  tmp=(*dk);
  (*dk)=tmp->suiv;
  (*dk)->prec=tmp->prec;
  *ct=tmp->ct;
  free(tmp);
  return VRAI;
}

// revoir
Booleen defilerEnTete(Deque *dk, Carte *ct){
  // MaillonD* tmp;
  //if(estDequeVide(*dk)) return FAUX;

  printf("\ndefilerT\n");
  
  // free(tmp);
  return VRAI;
}

// revoir
void afficherDequeTvQ(Deque dk){
  printf("\nTvQ\n");
}

// revoir
void afficherDequeQvT(Deque dk){
  printf("\nQvT\n");

}
