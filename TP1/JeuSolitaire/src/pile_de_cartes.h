// pile_de_cartes.h
#ifndef PILE_DE_CARTES_H
#define PILE_DE_CARTES_H

#include <stdio.h>
#include <stdlib.h>
#include "carte.h"

/////// structures
typedef struct maillon{
	Carte ct;
	struct maillon *suiv;
}Maillon, *PileC;

//////// fonctions
PileC creerPileVide(void);
void empiler(PileC *p, Carte ct);
Booleen depiler(PileC *p, Carte *ct);
Booleen estPileVide(PileC p);
Booleen sommetPile(PileC p, Carte *ct);
void afficherPile(PileC p);

#endif // PILE_DE_CARTES_H
