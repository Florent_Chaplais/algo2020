// DEQUE_DE_CARTES_H
#ifndef DEQUE_DE_CARTES_H
#define DEQUE_DE_CARTES_H

#include <stdio.h>
#include <stdlib.h>
#include "carte.h"


typedef struct maillonD{
	struct maillonD* prec;
	Carte ct;
	struct maillonD* suiv;
}MaillonD, *Deque;


Deque creerDequeVide(void);

Booleen estDequeVide(Deque dk);
void enfilerEnTete(Deque *dk, Carte ct);
void enfilerEnQueue(Deque *dk, Carte ct);
Booleen teteDeque(Deque dk, Carte *ct);
Booleen queueDeque(Deque dk, Carte *ct);
Booleen defilerEnQueue(Deque *dk, Carte *ct);
Booleen defilerEnTete(Deque *dk, Carte *ct);

void afficherDequeTvQ(Deque dk);
void afficherDequeQvT(Deque dk);

#endif // DEQUE_DE_CARTES_H
