/**
 * Fichier de test à décommenter au fur et à mesure
 */
/********************************************************************/



#include <stdio.h>
#include "pile_de_cartes.h"
#include "jeu_solitaire.h"
#include "deque_de_cartes.h"



//----------------------------------------------------------------------------
/**
 * permet de tester les piles de cartes 
 *   (creerPileVide, estPileVide, empiler, afficherPile, sommetPile, depiler) 
 */

void testPiles(void){
  PileC p = creerPileVide();
  Carte c, c1 = carteFaceVisible("R",'n',13);
  Carte c2 = carteFaceCachee("2",'r',2);
  Booleen ok;
  
  if(!estPileVide(p)) printf("PB : la pile devrait etre vide !!!");

  empiler(&p,c1);  
  empiler(&p,c2);

  printf("contenu de la pile : ");
  afficherPile(p); printf("\n");

  ok = sommetPile(p,&c);
  printf("sommet : "); afficherCarte(c); printf("\n");

  ok = depiler(&p,&c);
  if(!ok) printf("PB : on n'a pas pu dépilé : plus de carte\n");
  else{
    printf("carte dépilée : "); afficherCarte(c); printf("\n");
  }
  
  ok = depiler(&p,&c);
  if(!ok) printf("PB : on n'a pas pu dépilé : plus de carte\n");
  else{
    printf("carte dépilée : "); afficherCarte(c); printf("\n");
  }
  
  ok = depiler(&p,&c);
  if(!ok) printf("on n'a pas dépilé : plus de carte\n");
  else{
    printf("PB : on aurait pas du dépiler : "); afficherCarte(c); printf("\n");
  }
}

//----------------------------------------------------------------------------
/**
 * permet de tester les premières fonctions du jeu de cartes solitaire 
 *   (prendreLesCartes, melanger, creerJeuVide, distribuer, piocherCarte)
 */

void testPrendreLesCartes(void){
 PileC jeu = prendreLesCartes();

  afficherPile(jeu);
  printf("\n-----------------------\n");
}



void testMelanger(void){
 PileC jeu = prendreLesCartes();

  afficherPile(jeu);
  printf("\n-----------------------\n");

  melanger(&jeu);
  afficherPile(jeu);
  printf("\n-----------------------\n");
}


void testToutAfficher(void){
  Pioche pioche;
  QuatrePiles piles;
  creerJeuVide(piles, &pioche);
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");
  empiler(&pioche.talon, carteFaceCachee("10",'n',10));
  empiler(&pioche.talon, carteFaceCachee("R",'r',13));
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");
  empiler(&pioche.tas, carteFaceVisible("9",'n',9));
  empiler(&pioche.tas, carteFaceVisible("D",'r',12));
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");    
}


void testPiocherCarte(void){
  Pioche pioche;
  QuatrePiles piles;
  Carte c;
  Booleen reussi;
  creerJeuVide(piles, &pioche);
  empiler(&pioche.talon, carteFaceCachee("10",'n',10));
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");
  printf("on tente de piocher quand le talon n'est pas vide\n");
  reussi = piocherCarte(&pioche, &c);
  if(! reussi){
    printf("pb la carte n'a pas réussi à être piochée, elle aurait du\n");
  }
  else{
    printf("carte piochée : ");
    afficherCarte(c); printf("\n");
  }
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");
  printf("on tente de piocher quand le talon et le tas sont vides\n");
  reussi = piocherCarte(&pioche, &c);
  if(! reussi){
    printf("pas de carte piochée, c'est normal, la pioche est vide\n");
  }
  else{
    printf("carte piochée : ");
    afficherCarte(c);
    printf("  Pb: on aurait pas du pouvoir\n");
  }
  printf("\n-----------------------\n");
  printf("on met 3 cartes dans le tas de la pioche\n");
  empiler(&pioche.tas, carteFaceVisible("9",'n',9));
  empiler(&pioche.tas, carteFaceVisible("10",'n',10));
  empiler(&pioche.tas, carteFaceVisible("V",'n',11));
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");
  printf("on tente de piocher quand le talon est vide mais pas le tas\n");
  reussi = piocherCarte(&pioche, &c);
  if(! reussi){
    printf("pb la carte n'a pas réussi à être piochée, elle aurait du\n");
  }
  else{
    printf("carte piochée : ");
    afficherCarte(c); printf("\n");
  }
  toutAfficher(piles,pioche);
  printf("\n-----------------------\n");
}


//----------------------------------------------------------------------------
/**
 * permet de tester la fonction PoserCarteSurPile
 */
void testPoserCarteSurPile(void){
  QuatrePiles piles;
  Carte c1 = carteFaceVisible("1",'r',1);
  Carte c2 = carteFaceVisible("2",'n',2);
  Carte c3 = carteFaceVisible("2",'r',2);
  Carte c4 = carteFaceVisible("R",'r',13);
  int i;
  Booleen ok;
  char* message;
  for(i=0;i<4;i++){
    piles[i]=creerPileVide();
  }
  
  ok = poserCarteSurPile(piles,1,c2, &message);
  if(ok){
    printf("ERREUR : on n'aurait pas du pouvoir poser cette carte (ce n'est pas un as)!!!\n");
  }
  else printf(message);
  
  ok = poserCarteSurPile(piles,1,c1, &message);
  if(!ok){
    printf(message);
    printf("ERREUR : on aurait du pouvoir poser cette carte (c'est un as) !!!\n");
  }
    
  ok = poserCarteSurPile(piles,1,c2,&message);
  if(ok){
    printf("ERREUR : on n'aurait pas du pouvoir poser cette carte (pas la bonne couleur) !!!\n");
  }
  else printf(message);
  
  ok = poserCarteSurPile(piles,1,c3,&message);
  if(!ok){
    printf(message);
    printf("ERREUR : on aurait du pouvoir poser cette carte (bonne couleur et ordre croissant) !!!\n");
  }
    
  ok = poserCarteSurPile(piles,1,c4,&message);
  if(ok){
    printf("ERREUR : on n'aurait pas du pouvoir poser cette carte (pas la bonne valeur) !!!\n");
  }
  else printf(message);


  printf("contenu de la pile 1 : ");
  afficherPile(piles[1-1]);
  printf("\n");
}


//----------------------------------------------------------------------------
/**
 * permet de tester les deque de cartes
 *  (creerDequeVide, estDequeVide, enfilerEnTete, afficherDequeTvQ, afficherDequeQvT, teteDeque, 
 *  queueDeque, defilerEnQueue, enfilerEnQueue, defilerEnTete)
 */
 
void testDeque(void){
  Deque d = creerDequeVide();
  Carte c, c1 = carteFaceVisible("R",'n',13);
  Carte c2 = carteFaceVisible("2",'r',2);
  Booleen ok;
  
  if(!estDequeVide(d)) printf("ERREUR : la deque devrait etre vide !!!\n");
  
  enfilerEnTete(&d,c1);  
  enfilerEnTete(&d,c2);

  printf("contenu de la deque (Tete vers Queue) : ");
  afficherDequeTvQ(d); printf("\n");


  printf("contenu de la deque (Queue vers Tete) : ");
  afficherDequeQvT(d); printf("\n");


  ok = teteDeque(d,&c);
  printf("tete : "); afficherCarte(c); printf("\n");

  ok = queueDeque(d,&c);
  printf("queue : "); afficherCarte(c); printf("\n");
  

  ok = defilerEnQueue(&d,&c);
  if(!ok) printf("ERREUR : on n'a pas pu défiler en queue : plus de carte\n");
  else{
    printf("carte défilée en queue : "); afficherCarte(c); printf("\n");
  }
  
  ok = defilerEnQueue(&d,&c);
  if(!ok) printf("ERREUR : on n'a pas pu défiler en queue : plus de carte\n");
  else{
    printf("carte défilée en queue : "); afficherCarte(c); printf("\n");
  }
  
  ok = defilerEnQueue(&d,&c);
  if(!ok) printf("on n'a pas défilé en queue : plus de carte\n");
  else{
    printf("ERREUR : on n'aurait pas du défiler en queue: "); afficherCarte(c); printf("\n");
  }

  enfilerEnQueue(&d,c1);  
  enfilerEnQueue(&d,c2);

  printf("contenu de la deque (T vers Q) : ");
  afficherDequeTvQ(d); printf("\n");

  ok = defilerEnTete(&d,&c);
  if(!ok) printf("ERREUR : on n'a pas pu défiler en tete : plus de carte\n");
  else{
    printf("carte défilée en tete : "); afficherCarte(c); printf("\n");
  }
  
  ok = defilerEnTete(&d,&c);
  if(!ok) printf("ERREUR : on n'a pas pu défiler en tete : plus de carte\n");
  else{
    printf("carte défilée en tete : "); afficherCarte(c); printf("\n");
  }
  
  ok = defilerEnTete(&d,&c);
  if(!ok) printf("on n'a pas défilé en tete : plus de carte\n");
  else{
    printf("ERREUR : on n'aurait pas du défiler en tete: "); afficherCarte(c); printf("\n");
  }
}


//----------------------------------------------------------------------------
/**
 * permet de tester les fonctions modifiées CreerJeuVide et toutAfficher, ainsi que Distribuer
 */
/*
void testCreerJeuVideDistribuerToutAfficher(void){
  Pioche pioche;
  SeptColonnes colonnes;
  QuatrePiles piles;
  PileC lesCartes = prendreLesCartes();
      
  creerJeuVide(colonnes, piles, &pioche);
  printf("sur la table : \n");
  toutAfficher(colonnes,piles,pioche);
  printf("\n-----------------------------------------------\n");
  printf("les cartes sorties du paquet : \n");
  afficherPile(lesCartes);
  
  printf("\nles cartes mélangées : \n");
  melanger(&lesCartes);
  afficherPile(lesCartes);
  
  printf("\n-----------------------------------------------\n");
  distribuer(&lesCartes, colonnes, &pioche);
  printf("le paquet de cartes une fois distribuées : ");
  afficherPile(lesCartes);
  printf("et sur la table : \n");
  toutAfficher(colonnes,piles,pioche);
  printf("\n");

}
*/

//----------------------------------------------------------------------------

/**
 * permet de tester la fonction poserCarteSurColonne 
 */
/*
void testPoserCarteSurColonne(void){
  SeptColonnes colonnes;
  QuatrePiles piles;
  Pioche pioche;
  Booleen ok;
  char* message;
  Carte c1 = carteFaceVisible("V", 'n', 11);
  Carte c2 = carteFaceVisible("10", 'n', 10);
  Carte c3 = carteFaceVisible("D", 'r', 12);
  Carte c4 = carteFaceVisible("10", 'r', 10);

  printf("ce qu'il a sur la table (un jeu vide) : \n");
  creerJeuVide(colonnes, piles, &pioche);
  toutAfficher(colonnes, piles, pioche);
  printf("\n");

  printf("on veut poser Vn sur c1 \n");
  printf("tapez entrée\n");   scanf("%*c");
  
  ok=poserCarteSurColonne(colonnes, 1, c1,&message);
  if(!ok){
    printf(message);
    printf("ERREUR : on n'a pas pu poser une carte sur la colonne vide \n");
  }
  toutAfficher(colonnes, piles, pioche); printf("\n");

  printf("on veut poser 10n sur c1 \n");
  printf("tapez entrée\n");   scanf("%*c");
  
  ok=poserCarteSurColonne(colonnes, 1, c2,&message);
  if(ok){
    printf("ERREUR : on n'aurait pas du poser la carte sur la colonne car même couleur \n");
  }
  else printf(message);
  toutAfficher(colonnes, piles, pioche); printf("\n");

  printf("on veut poser Dr sur c1 \n");
  printf("tapez entrée\n");   scanf("%*c");

  
  ok=poserCarteSurColonne(colonnes, 1, c3,&message);
  if(ok){
    printf("ERREUR : on n'aurait pas du poser la carte sur la colonne car mauvaise valeur \n");
  }
  else printf(message);
  toutAfficher(colonnes, piles, pioche); printf("\n");

  printf("on veut poser 10r sur c1 \n");
  printf("tapez entrée\n");   scanf("%*c");

  ok=poserCarteSurColonne(colonnes, 1, c4,&message);
  if(!ok){
    printf(message);
    printf("ERREUR : on aurait du poser la carte sur la colonne \n");
  }
  toutAfficher(colonnes, piles, pioche); printf("\n");
}
*/
//----------------------------------------------------------------------------
/**
 * pour tester la fonction poserCartePiochee
 */
/*
void testPoserCartePiochee(void){
  SeptColonnes colonnes;
  QuatrePiles piles;
  Pioche pioche;
  char* message;
    
  creerJeuVide(colonnes, piles, &pioche);
  
  poserCarteSurColonne(colonnes,1,carteFaceVisible("10",'n',10), &message);
  poserCarteSurColonne(colonnes,2,carteFaceVisible("10",'r',10), &message);
  poserCarteSurColonne(colonnes,3,carteFaceVisible("5",'n',10), &message);
  poserCarteSurColonne(colonnes,4,carteFaceVisible("5",'r',10), &message);
  poserCarteSurColonne(colonnes,5,carteFaceVisible("3",'n',10), &message);
  poserCarteSurColonne(colonnes,6,carteFaceVisible("4",'r',10), &message);

  printf("ce qu'il a sur la table : \n");
  toutAfficher(colonnes, piles, pioche);
  printf("\n");

  printf("on veut poser 9n dans le jeu \n");
  printf("tapez entrée\n");   scanf("%*c");

  poserCartePiochee(colonnes, piles, &pioche, carteFaceVisible("9",'n',9));
  toutAfficher(colonnes, piles, pioche); printf("\n");


  printf("on veut poser 2n dans le jeu \n");
  printf("tapez entrée\n");   scanf("%*c");
  
  poserCartePiochee(colonnes, piles, &pioche, carteFaceVisible("2",'n',2));
  toutAfficher(colonnes, piles, pioche); printf("\n");


  printf("on veut poser 1n dans le jeu \n");
  printf("tapez entrée\n");   scanf("%*c");
  
  poserCartePiochee(colonnes, piles, &pioche, carteFaceVisible("1",'n',1));
  toutAfficher(colonnes, piles, pioche); printf("\n");


  printf("on veut poser 8n dans le jeu \n");
  printf("tapez entrée\n");   scanf("%*c");
  
  poserCartePiochee(colonnes, piles, &pioche, carteFaceVisible("8",'n',8));
  toutAfficher(colonnes, piles, pioche); printf("\n");

}  
*/


/**
 * permet de tester deplacerColonneSurColonne et deplacerCarteColonneVersPile
 */
/*
void testDeplacerColonne(void){
  SeptColonnes colonnes;
  QuatrePiles piles;
  Pioche pioche;
  Booleen ok;
  char de[3], vers[3];
  PileC lesCartes = prendreLesCartes();
  char* message;
  
  melanger(&lesCartes);

  creerJeuVide(colonnes, piles, &pioche);
  distribuer(&lesCartes,colonnes,&pioche);

  toutAfficher(colonnes, piles, pioche);
  printf("\n");

  while(1){
    printf("déplacement de où? (c1, c2, c3...., c7) : ");
    scanf("%s",de);
    printf("Vers où ? (c1, c2, c3...., c7 ou p1, ..., p4) : ");
    scanf("%s",vers);
    printf("%d vers %d\n",de[1]-48, vers[1]-48);
    if(vers[0]=='c'){
      printf("colonne\n");
      ok=deplacerColonneSurColonne(colonnes, de[1]-48, vers[1]-48, &message);
      if(ok){
	printf("colonne déplacée\n");
      }
      else{
	printf(message);
	printf("colonne non déplacée\n");
      }
      toutAfficher(colonnes, piles, pioche);
    }
    else{
      printf("pile\n");
      ok=deplacerCarteColonneVersPile(colonnes, de[1]-48, piles, vers[1]-48, &message);
      if(ok){
	printf("carte déplacée\n");
      }
      else{
	printf(message);
	printf("carte non déplacée\n");
      }
      toutAfficher(colonnes, piles, pioche);
    }
    printf("\n");
  }
}
*/



/**
 * permet de lancer le bon test ou alors la fonction jouer 
 */
int main(void){
  // testPiles();

  //testPrendreLesCartes();
  //testMelanger();
  //testToutAfficher();
  //testPiocherCarte();
  //testPoserCarteSurPile();
  testDeque();

  //testCreerJeuVideDistribuerToutAfficher();
  //testPoserCarteSurColonne();
  //testPoserCartePiochee();  
  //testDeplacerColonne();

  //jouer();	

  return 0;
}



