#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "jeu_solitaire.h"






/** 
 * fonction locale (qui n'a pas à apparaître dans le .h)  
 * cette fonction mélange les nbCartes cartes contenues dans le tableau tab
 */ 
void melangerTabCartes(Carte* tab, int nbCartes){
  int i, nbAleatoire;
  Carte tmp;
  srand(time(NULL));
  for(i=0; i<nbCartes; i++){
    nbAleatoire = rand()%nbCartes;
    tmp = tab[nbAleatoire];
    tab[nbAleatoire] = tab[i];
    tab[i] = tmp;
  }
}



///////////////  A COMPLETER
PileC prendreLesCartes(){
  Carte tmp;
  PileC pile= creerPileVide();
  char *TabNom[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "V", "D", "R"};
  int TabValeur[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
  char TabCouleur[3] = {'n','r','\0'};
  int i=0, nbCouleur=2, nbValeur=13;
  tmp=carteFaceVisible(TabNom[i%nbValeur],TabCouleur[i%nbCouleur],TabValeur[i%nbValeur]);
  empiler(&pile,tmp);
  for(i=1;i<=51;i++){
    tmp=carteFaceVisible(TabNom[i%nbValeur],TabCouleur[i%nbCouleur],TabValeur[i%nbValeur]);
    empiler(&pile,tmp);
  }
  return pile;
}

void melanger(PileC* jeu){
  int nbCartes=0;
  Carte tab[52];
  Booleen ok= depiler(jeu, &tab[nbCartes]);
  while(ok){
    nbCartes++;
    ok= depiler(jeu, &tab[nbCartes]);
		
  }
  melangerTabCartes(tab, nbCartes-1);
  for(int i=0;i<nbCartes;i++){
    empiler(jeu,tab[i]);
  }
}
  
void creerJeuVide(QuatrePiles piles, Pioche* pioche){
  for(int i=0; i<4;i++){
    piles[i]=NULL;
  }
  pioche->tas=NULL;
  pioche->talon=NULL;
}
 
void toutAfficher(QuatrePiles piles, Pioche pioche){
  printf("les piles : \n");
  for(int i=1;i<=4;i++){
    printf("p%d: ",i); afficherPile(piles[i-1]); printf("\n");
  }
  printf("tas: "); afficherPile((pioche).tas); printf("\n");
  printf("talon: "); afficherPile((pioche).talon); printf("\n");
}
 
Booleen piocherCarte(Pioche* pioche, Carte* c){
  if (pioche==NULL) return FAUX;
  if (depiler(&pioche->tas,c)){
    return VRAI;
  }
  else{
    return depiler(&pioche->talon,c);
  }
  return FAUX;
}
 
Booleen poserCarteSurPile(QuatrePiles piles, int num, Carte c, char** message){

  Carte sommet;
  Booleen ok= sommetPile(piles[num-1],&sommet);
  
  if(!ok && (c.val !=1)){
    *message="erreur: il faut un as\n";
    return FAUX;
  }

  if(!ok && (c.val ==1)){
    empiler(&piles[num-1],c);
    return VRAI;
  }
  
  if(strcmp(&c.couleur,&sommet.couleur)<0){
   *message="erreur: couleurs differentes\n";
    return FAUX;
  }
  
  if((c.val - sommet.val) > 1 ){
    *message="erreur: ordre croissant\n";
    return FAUX;
  }
  
  if(sommet.val == 13){
    *message="erreur: pile pleine\n";
    return FAUX;
  }
  
  empiler(&piles[num-1],c);
  return VRAI;
}
