#ifndef JEU_SOLITAIRE_H
#define JEU_SOLITAIRE_H

#include "pile_de_cartes.h"
// #include "deque_de_cartes.h"
#include <string.h>


/////// définitions de structures A FAIRE

typedef PileC QuatrePiles[4];

typedef struct {
  PileC tas;
  PileC talon;
} Pioche;

//////  FONCTIONS A CODER : 


PileC prendreLesCartes(void);

void melanger(PileC* jeu);

void creerJeuVide(QuatrePiles piles, Pioche* pioche);
//void creerJeuVide(SeptColonnes colonnes, QuatrePiles piles, Pioche* pioche);

void toutAfficher(QuatrePiles piles, Pioche pioche);
//void toutAfficher(SeptColonnes colonnes, QuatrePiles piles, Pioche pioche);

Booleen piocherCarte(Pioche* pioche, Carte* c);

Booleen poserCarteSurPile(QuatrePiles piles, int num, Carte c, char** message);

//void distribuer(PileC* lesCartes, SeptColonnes colonnes, Pioche* pioche);

//Booleen poserCarteSurColonne(SeptColonnes colonnes, int num, Carte c, char** message);

//void poserCartePiochee(SeptColonnes colonnes, QuatrePiles piles, Pioche* pioche, Carte c);

//Booleen deplacerColonneSurColonne(SeptColonnes colonnes, int de, int vers, char** message);

//Booleen deplacerCarteColonneVersPile(SeptColonnes colonnes, int de,QuatrePiles piles,int vers, char** message);

//Booleen jeuGagne(SeptColonnes colonnes, Pioche pioche);

//void jouer(void);





#endif // JEU_SOLITAIRE_H
