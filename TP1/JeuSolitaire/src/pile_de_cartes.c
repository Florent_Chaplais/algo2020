//pile_de_cartes.c
#include "pile_de_cartes.h"

PileC creerPileVide(){
  return NULL;
}

void empiler(PileC *p,Carte ct){
  Maillon *tmp;
  tmp=(Maillon*)malloc(sizeof(Maillon));
  if(tmp==NULL){
    printf("pb malloc \n");
    exit(1);
  }
  tmp->ct=ct;
  tmp->suiv=*p;
  *p=tmp;
}

Booleen estPileVide(PileC p){
    if(p==NULL) return VRAI;
    return FAUX;
}

Booleen depiler(PileC *p, Carte *ct){
  Maillon *tmp;
  if(estPileVide(*p)){
     return FAUX;
  }
  tmp= *p;
  *ct=tmp->ct;
  *p= tmp->suiv;
  free(tmp);
  return VRAI;
 }

Booleen sommetPile(PileC p, Carte *ct){
  if(estPileVide(p)) return FAUX;
  *ct=p->ct;
  return VRAI;
}

void afficherPile(PileC p){
  if(estPileVide(p)) return;
  afficherCarteVisible(p->ct);
  afficherPile(p->suiv);
}
