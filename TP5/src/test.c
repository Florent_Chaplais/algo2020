#include "reseau.h"

/// test reseau ///
void testReseau(){
  int retour;

  Reseau r= creerReseauVide();

  retour= ajouterSommet( r, 1);
  if(retour!=OUI) afficherCodeErreur(retour);
  else printf("ajouter 1");

  retour= ajouterSommet( r, 2);
  if(retour!=OUI) afficherCodeErreur(retour);
  else printf("ajouter 2");

  retour= existanceArc(r, 1, 2);
  if(retour!=OUI) printf("arc non existant");
  else printf("arc trouver");

  retour= ajouterArc(r, 1, 2);
  if(retour!=OUI) afficherCodeErreur(retour);
  else printf("ajouter arc 1--2");

  retour= existanceArc(r, 1, 2);
  if(retour!=OUI) printf("arc non existant");
  else printf("arc trouver");

  CellSommet *s1, *s2;
  s1= recherSommet(r,1);
  s2= rechercheSommet(r,2);

  retour= existanceArcPointe(s1,s2);
  if(retour!=OUI) printf("arc pointe non existant");
  else printf("arc pointe trouver");
}
/// ///

int main(void){

  testReseau();

  return 0;
}
