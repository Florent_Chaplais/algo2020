#ifndef RESEAU_H
#define RESEAU_H

#include<stdio>
#include<stdlib>

//peut être une enum//
#define NON 0
#define OUI 1
#define PB_MEMOIRE 2
#define PB_ARC_DEJA_EXISTANT 3
#define PB_SOMMET_DEP_NON_EXISTANT 4
#define PB_SOMMET_ARR_NON_EXISTANT 5
////
typedef enum {FAUX,VRAI}Booleen;

typedef struct cell Sommet{
  int val;
  struct cellArc* listeArcs;
  struct cellSommet* suiv;
  Booleen marque;
}CellSommet, *Reseau;

typedef struct cellArc{
  CellSommet* extremite;
  struct cellArc* suiv;
}CellArc; 

Reseau creerReseauVide(void);

CellSommet* rechercheSommet(Reseau r, int val);

int ajouterSommet(Reseau* ptr, int val);
int existanceArcPointe(CellSommet* pdep, CellSommet* ptarr);
int existanceArc(Reseau r, int dep, int arr);
int ajouterArc(Reseau r, int dep, int arr);
int suprimerArc(Reseau r, int dep, int arr);
int suprimerSommet(Reseau r, int val);
int compterNbNoeud(Reseau r);
int compterNbArc(Reseau r);

void afficherCodeErreur(int code);

void sauvegarderDans(Reseau r, File f);
void chargerDepuis(Reseau r, File f);
void exporterDot(Reseau r);

void estConnecter(Reseau r, int m1, int m2);

void toutDemarquer(Reseau r);
void parcoursProfondeur(Reseau r, int m1, int m2);
void communiqueAvec(Reseau r, int m1);

#endif
