#include "reseau.h"

Reseau creerReseauVide(){
  return NULL;
}

CellSommet* rechercheSommet( Reseau r, int val){
  if(r==NULL) return NULL;
  if(r->val==val) return r;
  return recheSommet( r->suiv, val);
}

int ajouterSommet( Reseau* ptr, int val){
  CellSommet* tmp;
  if(rechercheSommet( ptr, val)!=NULL){
    return PB_SOMMET_DEJA_EXISTANT;
  }
  tmp=(CellSommet*)malloc(sizeof(CellSommet));
  if(tmp==NULL) return PB_MEMOIRE;
  tmp->val=val;
  tmp->listArcs=NULL;
  tmp->suiv=*ptr;
  *ptr=tmp;
  return OUI;

  int existenceArcPointe( CellSommet* pdep, CellSommet* ptarr){
    CellArc* tmp=pdep->listArcs;
    while(tmp!=NULL){
      if(tmp->extremite==ptarr){
	return OUI;
      }
      tmp=tmp->suiv;
    }
    return NON;
  }

  int existanceArc( Reseau r, int dep, int arr){
    CellSommet *ptdep, *ptarr;
    ptdep= rechercheSommet( r, dep);
    if(ptdep==NULL){
      return PB_SOMMET_DEP_NON_EXISTANT;
    }
    ptarr= rechercheSommet( r, arr);
    if(ptarr==NULL){
      return PB_SOMMET_ARR_NON_EXISTANT;
    }
    return existanceArcPointer( ptdep, ptarr);
  }

  int ajouterArc( Reseau r, int dep, int arr){
    CellArc* tmp;
    CellSommet *ptdep, *ptarr;
    ptdep= rechercheSommet( r, dep);
    if( ptdep=NULL) return PB_SOMMET_DEP_NON_EXISTANT;
    ptarr= recherche(r, arr);
    if( ptarr=NULL) return PB_SOMMET_ARR_NON_EXISTANT;
    if( existanceArcPointer( ptdep, ptarr)) return PB_ARC_DEJA_EXISTANT;
    tmp=(CellArc*) malloc(sizeof(CellArc));
    if(tmp==NULL) return PB_MEMOIRE;
    tmp->extremite= ptarr;
    tmp->suiv= ptdep->listeArcs;
    ptdep->listeArcs=tmp;
    return OUI;
  }

  /// a finir ///
  int suprimerArc(Reseau r, int dep, int arr){

    return NON;
  }//6
  
  int suprimerSommet(Reseau r, int val){

    return NON;
  }//7
  
  int compterNbNoeud(Reseau r){

    return;
  }//8
  
  int compterNbArc(Reseau r){

    return;
  }//9
  
  /// ///
  
  void afficherCodeErreur(int code){
    switch( code){
    case NON:
      printf("non");
      break;
    case OUI:
      printf("oui");
      break;
    case PB_MEMOIRE:
      printf("probleme memoire");
      break;
    case PB_ARC_DEJA_EXISTANT:
      printf("arc deja existant");
      break;
    case PB_SOMMET_DEP_NON_EXISTANT:
      printf("sommet dep non existant");
      break;
    case PB_SOMMET_ARR_NON_EXISTANT:
      printf("sommet arr non existant");
      break;
    }
    default:
      printf("code erreur inconnu");
      break;
  }

  /// a finir ///

  void sauvegarderDans(Reseau r, File f){

    return;
  }//10
  
  void chargerDepuis(Reseau r, File f){

    return;
  }//10
  
  
  void exporterDot(Reseau r){

    return;
  }//11

  void estConnecter(Reseau r, int m1, int m2){
    CellSommet *pdep, *ptarr;
    int res1, res2;
    pdep=rechercherSommet(r, m1);
    ptarr=rechercherSommet(r, m2);
    res1= existanceArcPointe(pdep, ptarr);
    res2= existanceArcPointe(ptarr, pdep);
    if(res1==OUI) printf("%d vers %d\n",m1,m2);
    if(res2==OUI) printf("%d vers %d\n",m2,m1);
    if(res1==NON && res2==NON) printf("non connecté\n");
    return;
  }//12

  void toutDemarquer(Reseau r){
    if(r==NULL) return;
    r->marque=FAUX;
    toutDemarquer(r->suiv);
  }
  
  void parcoursProfondeur(Reseau r, int m1, int m2){
    int res= existanceArc(r, m1, m2);
    if(res==NON) printf("%d et %d ne communique pas.",m1,m2);
    else prinft("%d et %d communique.",m1,m2);
    return;
  }
  
  void communiqueAvec(Reseau r, int m1){
    CellArc *tmpa;
    CellSommet *v=rechercheSommet(r,m1);
    v->marque=VRAI;
    tmpa=v->listeArcs;
    printf("le sommet %d communique avec:\n ",v->val);
    while(tmpa!=NULL){
      v=tmpa->extremite;
      if(v->marque==FAUX){
	printf("%d \n",v->val);
	communiqueAvec(r, v);
      }
      tmpa=tmpa->suiv;
    }
    return;
  } //13

  /// ///
